<?php
declare(strict_types=1);
namespace LongCore\Crontab;


class LongCrontabScheduler
{
    /**
     * LongCrontabManage
     */
    protected LongCrontabManage $crontabManager;

    /**
     * \SplQueue
     */
    protected \SplQueue $schedules;

    /**
     * @param LongCrontabManage $crontabManager
     */
    public function __construct(LongCrontabManage $crontabManager)
    {
        $this->schedules = new \SplQueue();
        $this->crontabManager = $crontabManager;
    }

    public function schedule(): \SplQueue
    {
        foreach ($this->getSchedules() ?? [] as $schedule) {
            $this->schedules->enqueue($schedule);
        }
        return $this->schedules;
    }

    protected function getSchedules(): array
    {
        return $this->crontabManager->getCrontabList();
    }
}
