<?php
declare(strict_types=1);

namespace LongCore\Office;

use LongCore\LongModel;
use Psr\Http\Message\ResponseInterface;

interface ExcelPropertyInterface
{
    public function import(LongModel $model, ?\Closure $closure = null): bool;

    public function export(string $filename, array|\Closure $closure): ResponseInterface;
}