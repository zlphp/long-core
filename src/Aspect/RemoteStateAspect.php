<?php
declare(strict_types=1);

namespace LongCore\Aspect;

use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use LongCore\Annotation\RemoteState;
use LongCore\Exception\LongException;

#[Aspect]
class RemoteStateAspect extends AbstractAspect
{

    public array $annotations = [
        RemoteState::class
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     * @return mixed
     * @throws LongException
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $remote = $proceedingJoinPoint->getAnnotationMetadata()->method[RemoteState::class];
        if (!$remote->state) {
            throw new LongException('当前功能服务已禁止使用远程通用接口', 500);
        }

        return $proceedingJoinPoint->process();
    }
}