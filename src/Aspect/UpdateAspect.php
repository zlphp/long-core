<?php
declare(strict_types=1);

namespace LongCore\Aspect;

use Hyperf\Context\Context;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\Exception\Exception;
use LongCore\LongModel;
use LongCore\LongRequest;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ServerRequestInterface;

#[Aspect]
class UpdateAspect extends AbstractAspect
{
    public array $classes = [
        'LongCore\LongModel::update'
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     * @return mixed
     * @throws Exception
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $instance = $proceedingJoinPoint->getInstance();
        // 更新更改人
        if ($instance instanceof LongModel &&
            in_array('updated_by', $instance->getFillable()) &&
            config('longadmin.data_scope_enabled') &&
            Context::has(ServerRequestInterface::class) &&
            container()->get(LongRequest::class)->getHeaderLine('authorization')
        ) {
            try {
                $instance->updated_by = user()->getId();
            } catch (\Throwable $e) {
            }
        }
        return $proceedingJoinPoint->process();
    }
}