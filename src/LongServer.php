<?php
declare(strict_types=1);

namespace LongCore;

use Hyperf\HttpServer\Server;

class LongServer extends Server
{
    protected ?string $serverName = 'LongAdmin';

    protected $routes;

    public function onRequest($request, $response): void
    {
        parent::onRequest($request, $response);
        $this->bootstrap();
    }

    /**
     * LongServer bootstrap
     * @return void
     */
    protected function bootstrap(): void
    {
    }
}
